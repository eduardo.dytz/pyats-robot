from flask import Flask, current_app, request, render_template
from flask_admin import Admin
from flask_admin.contrib.fileadmin import FileAdmin
from flask_basicauth import BasicAuth
from jinja2 import Environment, FileSystemLoader
from subprocess import run
import os, sys, time, socket, glob, yaml
import os.path as op

app = Flask(__name__)

app.config['PYTHONPATH'] = '/static/'
app.config['ROOT_PATH'] = 'static'
app.config['FLASK_ADMIN_SWATCH'] = 'united'
app.secret_key = '12345'


admin = Admin(app, name='Logicalis', template_mode='bootstrap3')
path_scripts = op.join(op.dirname(__file__), 'static/files/scripts')
path_json = op.join(op.dirname(__file__), 'static/files/json')
path_inventory = op.join(op.dirname(__file__), 'static/files/inventory')
path_postman = op.join(op.dirname(__file__), 'static/files/postman')


class MyAdmin(FileAdmin):
    allowed_extensions = ('txt','yaml','j2','json')
    editable_extensions = ('txt','yaml','j2','json')
    can_download = True

class Postman(FileAdmin):
    can_download = True


admin.add_view(MyAdmin(path_scripts, name='Scripts',endpoint='scripts',category='Manager'))
admin.add_view(MyAdmin(path_inventory, name='Inventory',endpoint='inventory',category='Manager'))
admin.add_view(MyAdmin(path_json, name='Profiles',endpoint='profiles',category='Manager'))
admin.add_view(Postman(path_postman, name='Postman',endpoint='postman',category='Manager'))

app.config['BASIC_AUTH_USERNAME'] = 'logicalis'
app.config['BASIC_AUTH_PASSWORD'] = 'LOGICALIS'
basic_auth = BasicAuth(app)

def delete_robot_scripts():
    robot_files = glob.glob("static/files/scripts/*.robot")
    for robot_file in robot_files:
        os.remove(robot_file)


def create_directory_to_profiles(device,group,action):

    host = device
    group = group
    action = action

    group_folder = "static/files/json/{}".format(group)
    if not os.path.exists(group_folder):
        os.makedirs(group_folder)
    
    host_folder = "static/files/json/{}/{}".format(group,host)
    if not os.path.exists(host_folder):
        os.makedirs(host_folder)

    action_folder = "static/files/json/{}/{}/{}".format(group,host,action)
    if not os.path.exists(action_folder):
        os.makedirs(action_folder)


def get_command_line(action,workers):

    if action.lower() == 'snapshot':
        commands = ['pabot', '--processes', '{}'.format(workers), '--name', 'Snapshots',  '--outputdir', 'static/collect', 'static/files/scripts/*.robot']
    elif action.lower() == 'compare':
        commands = ['pabot', '--processes', '{}'.format(workers), '--name', 'Compare Snapshots', '--outputdir', 'static/collect', 'static/files/scripts/*.robot']
    else:
        return {"Response": "Action is invalid (snapshot or compare)"}
    return commands


def generate_robot_script(device,profiles,action,testbed):

    file_loader = FileSystemLoader('static/files/scripts')
    env = Environment(loader=file_loader)
    template = env.get_template('robot.j2')


    robot_script = template.render(device=device, profiles=profiles, action=action, testbed=testbed)
    robot_script_file = open("static/files/scripts/{}-{}.robot".format(testbed,device),'w')
    robot_script_file.write(str(robot_script))
    robot_script_file.close()
    create_directory_to_profiles(device=device,group=testbed,action=action)


def get_robot_script(devices_list=False, devices=False, profiles=False, action=False):

    profiles = profiles
    action = action

    if devices_list:
        for index in devices_list:
            device = devices_list[index]['device']
            testbed = devices_list[index]['group']
            generate_robot_script(device=device,profiles=profiles,action=action,testbed=testbed)
    else:
        for index in devices.keys():
            device = devices[index][0]['device']
            testbed = devices[index][1]['group']
            generate_robot_script(device=device,profiles=profiles,action=action,testbed=testbed)


def get_inventory_hosts(group=False):
    dict_hosts = {}
    inventory_files = glob.glob("static/files/inventory/*.yaml")
    index = 0
    for inventory_file in inventory_files:
        file_inventory = open(inventory_file,'r')
        parsed_inventory = yaml.load(file_inventory, Loader=yaml.FullLoader)
        groups = parsed_inventory['testbed']['name']
        if group:
            if group == groups:
                for host in parsed_inventory['devices'].keys():
                    dict_hosts.update({index:[]})
                    dict_hosts[index].append({'device':host})
                    dict_hosts[index].append({'group':group})
                    index += 1
        else:
            for host in parsed_inventory['devices'].keys():
                dict_hosts.update({index:{}})
                dict_hosts[index].update({'device':host})
                dict_hosts[index].update({'group':groups})
                index += 1
    return dict_hosts


@basic_auth.required
@app.route("/admin/fileadmin")


@app.route("/")
@app.route("/report.html")
@basic_auth.required
def report_page():
    return current_app.send_static_file('collect/report.html')


@app.route("/log.html")
@basic_auth.required
def log_page():
    return current_app.send_static_file('collect/log.html')


@app.route("/run", methods=['POST'])
def exec_from_api():

    delete_robot_scripts()

    data = request.get_json()

    profiles = data['profiles']
    action = data['action']
    workers = data['workers']

    for key, value in data.items():
        if key == 'devices':
            devices_list = data['devices']
        elif key == 'groups':
            groups = data['groups']

    if 'groups' in locals():
        for group in groups:
            get_devices = get_inventory_hosts(group=group)
            get_robot_script(devices=get_devices,profiles=profiles,action=action)
    
    if 'devices_list' in locals():
        get_devices = {}
        index = 0
        for device in devices_list:
            host = device.split('.')[1]
            group = device.split('.')[0]
            get_devices.update({index:{}})
            get_devices[index].update({'device':host})
            get_devices[index].update({'group':group})
            index += 1
        get_robot_script(devices_list=get_devices,profiles=profiles,action=action)

    command_line = get_command_line(action=action,workers=workers)

    if command_line:
        run(command_line)
        return str(get_devices)


@app.route('/form', methods=['GET'])
def my_form():
    dict_hosts = get_inventory_hosts()
    hosts = []
    groups = set([])
    for index in dict_hosts.keys():
        host = '{}.{}'.format(dict_hosts[index]['group'], dict_hosts[index]['device'])
        hosts.append(host)
        groups.add(dict_hosts[index]['group'])
    return render_template('form.html', groups=groups, hosts=hosts)


@app.route('/form', methods=['POST'])
def my_form_post():

    delete_robot_scripts()

    index = 0
    profiles = request.form.getlist('profiles')
    action = request.form['action']
    workers = request.form['workers']
    selection = request.form['select_type']
    if selection == 'group':
        groups = request.form.getlist('groups')
        for group in groups:
            devices = get_inventory_hosts(group=group)
            get_robot_script(devices=devices,profiles=profiles,action=action)
    else:
        devices_list = request.form.getlist('hosts')
        devices = {}
        for device in devices_list:
            host = device.split('.')[1]
            group = device.split('.')[0]
            devices.update({index:{}})
            devices[index].update({'device':host})
            devices[index].update({'group':group})
            index += 1
        get_robot_script(devices_list=devices,profiles=profiles,action=action)

    command_line = get_command_line(action=action,workers=workers)

    run(command_line)

    return str(devices)

@app.route("/help.html")
def help_page():
    return render_template('help.html')


app.run(host="0.0.0.0", port=80)